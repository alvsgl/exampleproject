﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonsManager : MonoBehaviour {

    public System.Action OnAdsButton;

    [SerializeField]
    private Button adsButton;

    [SerializeField]
    private Button goodsButton;

    private void Start()
    {
        goodsButton.interactable = false;
    }

    public void AdsButtonHandler()
    {
        if (OnAdsButton != null)
        {
            OnAdsButton();
        }
        goodsButton.interactable = true;
    }

    public void GoodsButtonHandler()
    {
        goodsButton.interactable = false;
    }

}
