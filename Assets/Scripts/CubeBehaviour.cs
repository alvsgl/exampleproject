﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeBehaviour : MonoBehaviour {

    // Действие - Клик по кубику. Cообщает количество очков за куб
    public System.Action<int> OnCubeClickScores;

    // Стоимость куба в очках
    [SerializeField]
    private int scores;

    // Цикл который вызывается системой каждый кадр
    private void Update()
    {
        //Проверяем на клик левой кнопки мыши
        if (Input.GetMouseButtonDown(0))
        {
            MouseClick();
        }
    }

    private void MouseClick()
    {

        // Делаем луч из позиции мыши в направлении камеры, чтобы проверить не попали ли мы в наш куб
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            // Если попали в наш куб
            if (hit.transform.gameObject == gameObject)
            {
                // Если кто-то подписан на действие
                if (OnCubeClickScores != null)
                {
                    // Сообщаем количество очков за этот куб
                    OnCubeClickScores(scores);

                    // Удаляем все подписки
                    OnCubeClickScores = null;

                    // Уничтожаем наш куб
                    Destroy(gameObject);
                }
            }
        }
    }

}
