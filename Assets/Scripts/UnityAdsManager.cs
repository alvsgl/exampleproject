﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;



public class UnityAdsManager : MonoBehaviour, IAds
{
    public System.Action OnGameInterruption;
    public System.Action OnInterruptionAdShown;

    private bool timeToShowNonskippabele;

    private void Start()
    {
        // инициализация рекламного модуля
        Advertisement.Initialize("a8c7d6e7-59b9-4bb5-a8f0-6c5c6b6a3dd2", true);
    }

    public void GameStarted()
    {
        //Запускаем цикл показа рекламы каждые 15 минут
        StartCoroutine(ShowAdEvery15Minutes());
    }

    public void ShowAdOnScoreChange()
    {
        if (timeToShowNonskippabele)
        {
            StartCoroutine(ShowAdWhenReady(true));
            timeToShowNonskippabele = false;
        }
        else
        {
            StartCoroutine(ShowAdWhenReady());
        }

    }

    //если реклама не подгрузилась ждем и потом показываем
    IEnumerator ShowAdWhenReady(bool rewarded = false)
    {
        if (rewarded)
        {
            while (!Advertisement.IsReady("rewardedVideo"))
                yield return null;

            Advertisement.Show("rewardedVideo");

        }
        else
        {

            while (!Advertisement.IsReady())
                yield return null;

            Advertisement.Show();
        }
    }



    public void ShowUnlockingAd()
    {
        StartCoroutine(ShowAdWhenReady(true));
    }

    private IEnumerator ShowAdEvery15Minutes()
    {
        // Ждем 15 минут
        yield return new WaitForSeconds(15f * 60f);
        // Если надо остановить геймплей перед показом рекламы
        if (OnGameInterruption != null)
        {
            OnGameInterruption();
        }
        //время показать нескрываемую рекламу
        timeToShowNonskippabele = true;


        StartCoroutine(ShowAdEvery15Minutes());
    }


}
