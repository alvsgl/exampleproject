﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;

public class AdsManager : MonoBehaviour , IBannerAdListener, IInterstitialAdListener, INonSkippableVideoAdListener, IAds
{

    public System.Action OnGameInterruption;
    public System.Action OnInterruptionAdShown;

    private bool timeToShowNonskippabele;

    private void Start()
    {
        // инициализация рекламного модуля

        string appKey = "8378e98ca2ef5cd9f4c53b545f20e0db312255771a7209a3";
        Appodeal.initialize(appKey, Appodeal.BANNER | Appodeal.INTERSTITIAL | Appodeal.NON_SKIPPABLE_VIDEO);
        Appodeal.disableLocationPermissionCheck();
        Appodeal.disableWriteExternalStoragePermissionCheck();

        Appodeal.setTriggerOnLoadedOnPrecache(Appodeal.INTERSTITIAL, true);

        Appodeal.setSmartBanners(true);
        Appodeal.setBannerAnimation(false);
        Appodeal.setTabletBanners(false);
        Appodeal.setBannerBackground(false);

        Appodeal.setChildDirectedTreatment(false);
        Appodeal.muteVideosIfCallsMuted(true);
        Appodeal.setAutoCache(Appodeal.INTERSTITIAL, false);

        Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER_VIEW | Appodeal.NON_SKIPPABLE_VIDEO);

        Appodeal.setBannerCallbacks(this);
        Appodeal.setInterstitialCallbacks(this);
        Appodeal.setNonSkippableVideoCallbacks(this);

    }


    public void GameStarted()
    {
        //Стартуем баннер
        Appodeal.show(Appodeal.BANNER_TOP, "banner_button_click");

        //Запускаем цикл показа рекламы каждые 15 минут
        StartCoroutine(ShowAdEvery15Minutes());
    }

    public void ShowAdOnScoreChange()
    {
        if (timeToShowNonskippabele)
        {
            showNonSkippable();
            timeToShowNonskippabele = false;
        }
        else
        {
            showInterstitial();
        }

    }

    public void ShowUnlockingAd()
    {
        showNonSkippable();
    }

    public void showInterstitial()
    {
        if (Appodeal.isLoaded(Appodeal.INTERSTITIAL) && !Appodeal.isPrecache(Appodeal.INTERSTITIAL))
        {
            Appodeal.show(Appodeal.INTERSTITIAL);
        }
        else
        {
            Appodeal.cache(Appodeal.INTERSTITIAL);
        }
    }

    public void showNonSkippable()
    {
        if (Appodeal.isLoaded(Appodeal.NON_SKIPPABLE_VIDEO) && !Appodeal.isPrecache(Appodeal.NON_SKIPPABLE_VIDEO))
        {
            Appodeal.show(Appodeal.NON_SKIPPABLE_VIDEO);
        }
        else
        {
            Appodeal.cache(Appodeal.NON_SKIPPABLE_VIDEO);
        }
    }


    private IEnumerator ShowAdEvery15Minutes()
    {
        // Ждем 15 минут
        yield return new WaitForSeconds(15f * 60f);
        // Если надо остановить геймплей перед показом рекламы
        if (OnGameInterruption != null)
        {
            OnGameInterruption();
        }
        //время показать нескрываемую рекламу
        timeToShowNonskippabele = true;


        StartCoroutine(ShowAdEvery15Minutes());
    }

    void OnApplicationFocus(bool hasFocus)
    {
        if (hasFocus)
        {
            Appodeal.onResume();
        }
    }

    #region Banner callback handlers
    public void onBannerLoaded(bool precache) { }
    public void onBannerFailedToLoad() { print("banner failed"); }
    public void onBannerShown() { print("banner opened"); }
    public void onBannerClicked() { print("banner clicked"); }
    #endregion

    #region Interstitial callback handlers
    public void onInterstitialLoaded(bool isPrecache) { print("Interstitial loaded"); }
    public void onInterstitialFailedToLoad() { print("Interstitial failed"); }
    public void onInterstitialShown() { print("Interstitial opened"); }
    public void onInterstitialClosed() { print("Interstitial closed"); }
    public void onInterstitialClicked() { print("Interstitial clicked"); }
    #endregion

	#region Non Skippable Video callback handlers
	public void onNonSkippableVideoLoaded() { Debug.Log("NonSkippable Video loaded"); }
	public void onNonSkippableVideoFailedToLoad() { Debug.Log("NonSkippable Video failed to load"); }
	public void onNonSkippableVideoShown() { Debug.Log("NonSkippable Video opened"); }
	public void onNonSkippableVideoClosed(bool isFinished) { Debug.Log("NonSkippable Video, finished:" + isFinished); }
	public void onNonSkippableVideoFinished() { Debug.Log("NonSkippable Video finished"); }
	#endregion



}
