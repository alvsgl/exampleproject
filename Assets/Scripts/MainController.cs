﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainController : MonoBehaviour {

    [SerializeField]
    private GameExample game;
    [SerializeField]
    private GameObject ads;
    [SerializeField]
    private ButtonsManager buttons;

    [SerializeField]
    private UnityEngine.UI.Text scoresText;

    private IAds adsManager;

    private int lastScore = 0;

    private void Start()
    {

        adsManager = ads.GetComponent<IAds>();

        // Подписываемся на событие получения очков
        game.OnScoresChange += ScoresChangeHandler;

        // Подписываемся на рекламную кнопку 
        buttons.OnAdsButton += AdsButtonHandler;

        // Запускаем игру
        game.Initialize();

        // Инициализируем рекламу
        adsManager.GameStarted();
    }

    private void ScoresChangeHandler(int scores)
    {

        // Устанавливаем новый текст подсчета очков
        scoresText.text = "Score: " + scores.ToString();

        //если достигли порога в 100 очков - показываем рекламу
        if (scores - lastScore >= 100)
        {
            lastScore = Mathf.RoundToInt(Mathf.RoundToInt(scores / 100f) * 100f);

            // Показываем рекламу
            adsManager.ShowAdOnScoreChange();
        }


    }

    private void AdsButtonHandler()
    {
        adsManager.ShowUnlockingAd();
    }


}
