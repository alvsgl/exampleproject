﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAds {
    void GameStarted();
    void ShowUnlockingAd();
    void ShowAdOnScoreChange();

}
