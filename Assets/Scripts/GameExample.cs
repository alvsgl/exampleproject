﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameExample : MonoBehaviour {

    // Кубики которые будут учавствовать в это игре
    [SerializeField]
    private CubeBehaviour[] cubes;

    // Действие сообщающее что счет изменился и возвращающее текущий счет
    public System.Action<int> OnScoresChange; 

    // Количество очков 
    private int scores = 0;

    // Создаем уровень
    public void Initialize()
    {
        // Добавим на поле пять случайных кубиков
        for (int i = 0; i < 5; i++)
        {
            AddRandomCube();
        }

        // Запускаем сопрограмму, которая раз в две секунды будет добавлять случайные кубы
        StartCoroutine(MakeCubeCoroutine());
    }

    // Создание случайного куба в случайном месте
    private void AddRandomCube()
    {
        int randomCube = Random.Range(0, cubes.Length);
       
        // Создаем клон объекта
        GameObject go = Instantiate(cubes[randomCube].gameObject);
        // Находим компонент куба
        CubeBehaviour cube = go.GetComponent<CubeBehaviour>();
        // Подписываемся на событие клика и при клике вызовется наш AddScores куда будет передано количество очков за куб
        cube.OnCubeClickScores += AddScores;

        // Размещаем случайным образом
        int randomX = Random.Range(-10, 11);
        int randomZ = Random.Range(-10, 11);
        go.transform.position = new Vector3(randomX, 0, randomZ);
    }

    // Сопрограмма которая генерирует кубы раз в две секунды
    private IEnumerator MakeCubeCoroutine()
    {
        // Ждем две секунды
        yield return new WaitForSeconds(2f);
        // Добавим на поле три случайных кубика
        for (int i = 0; i < 3; i++)
        {
            AddRandomCube();
        }
        // Запускаем сопрограмму заново
        StartCoroutine(MakeCubeCoroutine());
    }

    // Добавляем требуемое количество очков
    private void AddScores(int amount)
    {
        scores += amount;
        // Если кто-то подписан на действие, то выполняем подписки и передаем в них количество очков на данный момент
        if (OnScoresChange != null)
        {
            OnScoresChange(scores);
        }
    }

}
